//
//  AWLOptionsController.h
//  AWLColorPicker
//
//  Created by Svitlana on 07.05.14.
//  Copyright (c) 2014 WaveLabs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AWLOptionsController : NSWindowController
- (IBAction)closeOptionsWindow:(id)sender;
@end
